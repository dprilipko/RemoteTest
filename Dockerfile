# specify the node base image with your desired version node:<version>
FROM node:alpine

RUN apk add --no-cache bash git
COPY . /app
WORKDIR /app
RUN yarn

# replace this with your application's default port
EXPOSE 8081

CMD ["yarn", "start"]