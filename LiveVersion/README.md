
# react-native-live-version

## Getting started

`$ npm install react-native-live-version --save`

### Mostly automatic installation

`$ react-native link react-native-live-version`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-live-version` and add `RNLiveVersion.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNLiveVersion.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNLiveVersionPackage;` to the imports at the top of the file
  - Add `new RNLiveVersionPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-live-version'
  	project(':react-native-live-version').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-live-version/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-live-version')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNLiveVersion.sln` in `node_modules/react-native-live-version/windows/RNLiveVersion.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Live.Version.RNLiveVersion;` to the usings at the top of the file
  - Add `new RNLiveVersionPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNLiveVersion from 'react-native-live-version';

// TODO: What to do with the module?
RNLiveVersion;
```
  