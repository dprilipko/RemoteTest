import {NativeModules} from "react-native";

const { RNLiveVersion, RNLiveVersionEventEmitter } = NativeModules;
export { RNLiveVersion, RNLiveVersionEventEmitter };
export const EVENTS = {
    TOGGLE: 'toggleLiveVersionView',
};

export const PROJECT = process.env.PROJECT || 'remote-test';
export const SERVER = process.env.SERVER || 'http://192.168.2.137:8000';
export const [BUNDLER_HOST, BUNDLER_PORT] = (process.env.BUNDLER || '192.168.2.137:8081').split(':');
