import React from 'react';
import {
    NativeEventEmitter, AppRegistry, Platform,
    Modal, View, Text, TouchableOpacity,
} from 'react-native';
import { RNLiveVersion, RNLiveVersionEventEmitter, BUNDLER, SERVER, EVENTS, PROJECT } from './config';
import styles from './styles';
import {createStackNavigator, HeaderBackButton} from "react-navigation";
import ListVersions from './listVersions';

export default class LiveVersionApp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: true
        };
    }

    componentWillMount() {
        this.subscription = new NativeEventEmitter(RNLiveVersionEventEmitter).addListener(EVENTS.TOGGLE, () => {
            this.setState({ modalVisible: !this.state.modalVisible });
        });

        fetch(`${SERVER}/handshake?project=${PROJECT}`);
    }

    componentWillUnmount() {
        this.subscription.remove();
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => this.setState({ modalVisible: false })}>
                <RootStack />
            </Modal>
        );
    }
}

const RootStack = createStackNavigator(
    {
        listVersions: {
            screen: ListVersions,
            navigationOptions: ({ navigation }) => ({
                headerLeft: <HeaderBackButton title="Close"
                                              onPress={() => new NativeEventEmitter(RNLiveVersionEventEmitter).emit(EVENTS.TOGGLE)} />,
            }),
        },
    },
    {
        mode: 'card',
    }
);

AppRegistry.registerComponent('LiveVersion', () => LiveVersionApp);