#if __has_include("RCTDevMenu.h")
#import "RCTDevMenu.h"
#else
#import <React/RCTDevMenu.h>
#endif

@interface RNLiveVersion : NSObject <RCTBridgeModule>

@property (nonatomic, weak, readonly) RCTBridge *mainAppBridge;
@property (nonatomic, strong, readonly) RCTDevMenuItem *devMenuItem;

+ (RNLiveVersion *)sharedInstance;

@end
