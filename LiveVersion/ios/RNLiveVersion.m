
#import "RNLiveVersion.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <React/RCTBridge+Private.h>

static NSString *const kBundleHostKey = @"kBundleHostKey";
static NSString *const kBundlePortKey = @"kBundlePortKey";

@interface RCTBundleURLProvider (Custom)
@end
@implementation RCTBundleURLProvider (Custom)
static NSURL *serverRootWithHost(NSString *host)
{
    NSString *cHost = [[NSUserDefaults standardUserDefaults] stringForKey:kBundleHostKey];
    NSString *port = [[NSUserDefaults standardUserDefaults] stringForKey:kBundlePortKey];
    if (cHost && port) {
        return [NSURL URLWithString:
                [NSString stringWithFormat:@"http://%@:%@/",
                 cHost, port]];
    }

    return [NSURL URLWithString:
            [NSString stringWithFormat:@"http://%@:%lu/",
             host, (unsigned long)kRCTBundleURLProviderDefaultPort]];
}
@end

@interface BundleURLProvider : RCTBundleURLProvider
@end
@implementation BundleURLProvider
- (BOOL)enableDev
{
    return NO;
}

- (BOOL)enableLiveReload
{
    return NO;
}

- (BOOL)enableMinification
{
    return YES;
}
@end

@implementation RNLiveVersion
@synthesize devMenuItem = _devMenuItem, bridge = _bridge, mainAppBridge = _mainAppBridge;

static RNLiveVersion *_sharedInstance;
+ (RNLiveVersion *)sharedInstance {
    return _sharedInstance;
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (instancetype)init
{
    // We're only overriding this to ensure the module gets created at startup
    // TODO (t11106126): Remove once we have more declarative control over module setup.
    if (self = [super init]) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            NSURL *jsCodeLocation = [[BundleURLProvider new] jsBundleURLForBundleRoot:@"LiveVersion/index" fallbackResource:nil];
            RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                                moduleName:@"LiveVersion"
                                                         initialProperties:nil
                                                             launchOptions:@{}];

            [[UIApplication sharedApplication].delegate.window.rootViewController.view addSubview:rootView];

            _sharedInstance = self;
        });
    }

    return self;
}

- (void)setBridge:(RCTBridge *)bridge
{
    _bridge = bridge;
#if __has_include(<React/RCTDevMenu.h>) || __has_include("RCTDevMenu.h")
    [_bridge.devMenu addItem:self.devMenuItem];

    RCTRootView *rootView = (RCTRootView *)[UIApplication sharedApplication].delegate.window.rootViewController.view;
    _mainAppBridge = rootView.bridge;
#endif
}

RCT_EXPORT_MODULE()

- (RCTDevMenuItem *)devMenuItem
{
    if (!_devMenuItem) {
        _devMenuItem = [RCTDevMenuItem buttonItemWithTitleBlock:^NSString *{
            return @"Live Version";
        } handler:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"toggleLiveVersionView" object:nil];
        }];
    }

    return _devMenuItem;
}

RCT_EXPORT_METHOD(reloadBundle:(nonnull NSString *)host port:(nonnull NSString *)port)
{
    [[NSUserDefaults standardUserDefaults] setObject:host forKey:kBundleHostKey];
    [[NSUserDefaults standardUserDefaults] setObject:port forKey:kBundlePortKey];

    [_mainAppBridge reload];
}
@end
