
Pod::Spec.new do |s|
  s.name         = "RNLiveVersion"
  s.version      = "1.0.0"
  s.summary      = "RNLiveVersion"
  s.description  = <<-DESC
                  RNLiveVersion
                   DESC
  s.homepage     = ""
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/author/RNLiveVersion.git", :tag => "master" }
  s.source_files  = "RNLiveVersion/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

  