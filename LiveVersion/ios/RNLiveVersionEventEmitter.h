#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RNLiveVersionEventEmitter : RCTEventEmitter <RCTBridgeModule>

@end
