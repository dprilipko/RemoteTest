#import "RNLiveVersionEventEmitter.h"

@implementation RNLiveVersionEventEmitter

RCT_EXPORT_MODULE();

+ (BOOL)requiresMainQueueSetup
{
    // UIApplication.applicationState seems reasonably safe to access from
    // a background thread.
    return NO;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (NSArray<NSString *> *)supportedEvents
{
    return @[@"toggleLiveVersionView"];
}

- (void)receiveTestNotification
{
    [self sendEventWithName:@"toggleLiveVersionView" body:nil];
}

- (void)startObserving
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification)
                                                 name:@"toggleLiveVersionView"
                                               object:nil];
}
- (void)stopObserving
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
