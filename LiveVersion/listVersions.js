import React from 'react';
import {
    Dimensions,
    Platform,
    Button,
    FlatList,
    Text,
    View,
    StatusBar, NativeEventEmitter,
} from 'react-native';
import {RNLiveVersion, RNLiveVersionEventEmitter, EVENTS, PROJECT, SERVER, BUNDLER_HOST, BUNDLER_PORT} from './config';

const Separator = () => (
    <View
        style={{
            width: Dimensions.get('window').width - 100,
            height: 1,
            backgroundColor: '#ccc',
            marginHorizontal: 50,
            marginTop: 15,
            marginBottom: 15,
        }}
    />
);

const Spacer = () => (
    <View
        style={{
            marginBottom: Platform.OS === 'android' ? 20 : 5,
        }}
    />
);

export default class ListVersions extends React.Component {
    state = {
        items: [],
    };

    componentWillMount() {
        fetch(`${SERVER}/branches?project=${PROJECT}`)
            .then(res => console.log('1 >>', res)||res.json())
            .then(this.pollForBranches);
    }

    pollForBranches = ({url}) => setTimeout(() => {
        fetch(url)
            .then(res => console.log('2 >>', res)||res.json())
            .then((data = {}) => console.log("data = ", data)||data)
            .then(({branches = {}}) => this.setState({items: Object.values(branches)}))
            .catch(() => this.pollForBranches({url}));
    }, 1000);

    switchBranch(item) {
        fetch(`${SERVER}/handshake?project=${PROJECT}&forced=true&branch=${item.name}`)
            .then(() => RNLiveVersion.reloadBundle(BUNDLER_HOST, BUNDLER_PORT))
    }

    renderItem = ({item}) => (<Button title={item.current ? `* ${item.name}` : item.name} onPress={() => this.switchBranch(item)}/>)

    render() {
        return (
            <FlatList style={{ flex: 1 }}
                      data={this.state.items}
                      keyExtractor={item => item.name}
                      renderItem={this.renderItem}
            />
        );
    }

    _goBack = () => {
        new NativeEventEmitter(RNLiveVersionEventEmitter).emit(EVENTS.TOGGLE);
    };

    _hideStatusBar = () => {
        StatusBar.setHidden(true, 'slide');
    };

    _showStatusBar = () => {
        StatusBar.setHidden(false, 'slide');
    };
};