using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Live.Version.RNLiveVersion
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNLiveVersionModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNLiveVersionModule"/>.
        /// </summary>
        internal RNLiveVersionModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNLiveVersion";
            }
        }
    }
}
